﻿using System;
using System.Linq;
using System.IO;
using CsvHelper;
using System.Globalization;
using iTextSharp.text.pdf;
using iTextSharp.text;
using RestSharp;
using System.Configuration;
using System.Net;
using IBM.Data.DB2.iSeries;
using System.Data;
using System.Threading;
using log4net;

namespace PDFReprints
{
    class PDFReprints
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["csvDir"]);
            var files = di.GetFiles(ConfigurationManager.AppSettings["sFiles"]);
            if (files.Count() > 0)
            {
                using (var reader = new StreamReader(files[0].FullName))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    string fileName = string.Empty;
                    var record = new Reprint();
                    var records = csv.EnumerateRecords(record);
                    string legalLoc = ConfigurationManager.AppSettings["legalLoc"];
                    string letterLoc = ConfigurationManager.AppSettings["letterLoc"];
                    //CentricityApi centricityApi = new CentricityApi();
                    string sConfig = string.Empty;
                    log.Info($"...Preparing to check all records...");
                    foreach (var r in records)
                    {

                        fileName = ConfigurationManager.AppSettings["contractsLoc"] + r.CONTRACT + ".pdf";
                        log.Info($"...Checking {fileName} file...");
                        //Logger_NoDealerId.LogMessage($"...Checking {fileName} file...");
                        // r is the same instance as record. 
                        if (File.Exists(fileName))
                        {
                            log.Info($"...found {fileName} file...");
                            PdfReader pdfReader = new PdfReader(fileName);
                            //Rectangle mediabox = pdfReader.GetPageSize(1);
                            Rectangle cropbox = pdfReader.GetCropBox(1);
                            if (cropbox.Height == 1008)
                            {
                                log.Info($"...copying {fileName} file to {legalLoc}");
                                File.Copy(fileName, legalLoc + r.CONTRACT + ".pdf", true);
                            }
                            else
                            {
                                log.Info($"...copying {fileName} file to {letterLoc}");
                                File.Copy(fileName, letterLoc + r.CONTRACT + ".pdf", true);
                            }                                
                        }
                        else
                        {
                            if (!fileName.Contains("MC"))
                            {
                                log.Info($"...file was not found so we will create {fileName} file, wait 50 sec and check again...");
                                var companyName = GetCompanyName(r.CONTRACT);
                                //string result = ExecuteHttp($"http://www.bankerswarrantygroup.com/Webservices/TandCWeb/Default.aspx?P={companyName}{r.CONTRACT}");
                                string apiurl = ConfigurationManager.AppSettings["apiURL"] + companyName + r.CONTRACT;
                                string result = ExecuteHttp(apiurl);                                 
                                Thread.Sleep(50000);
                                //Task.Delay(TimeSpan.FromMinutes(1.5));
                                if (File.Exists(fileName))
                                {
                                    PdfReader pdfReader = new PdfReader(fileName);
                                    //Rectangle mediabox = pdfReader.GetPageSize(1);
                                    Rectangle cropbox = pdfReader.GetCropBox(1);
                                    if (cropbox.Height == 1008)
                                    {
                                        log.Info($"...after file was created copying {fileName} file to {legalLoc}");
                                        File.Copy(fileName, legalLoc + r.CONTRACT + ".pdf", true);
                                    }
                                        
                                    else
                                    {
                                        log.Info($"...after file was created copying {fileName} file to {letterLoc}");
                                        File.Copy(fileName, letterLoc + r.CONTRACT + ".pdf", true);
                                    }
                                        
                                }
                            }
                        }
                    }                   
                }
                // move file to PRCD folder
                string NewName = files[0].Name;
                NewName = NewName.Replace(".csv", "_Processed.csv");
                string NewNamePath = ConfigurationManager.AppSettings["csvprcdDir"] + NewName;
                File.Copy(files[0].FullName, NewNamePath,true);
                File.Delete(files[0].FullName);
            }

        }
        public static IRestResponse Execute(string url, string apiTimeOut)
        {
            var stringSplit = "api/";

            var urlArray = url.Split(new[] { stringSplit }, StringSplitOptions.None);
            var apiBaseUrl = $"{urlArray[0]}{stringSplit}";
            var client = new RestClient(apiBaseUrl);
            int minutes = Int32.Parse(apiTimeOut);
            client.Timeout = minutes * 60 * 1000;

            var request = new RestRequest(Method.POST);
            request.Resource = urlArray[1];
            //request.RootElement = "dealer";

            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Accept", "application/json");


            var response = client.Execute(request);

            if (!response.IsSuccessful && response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                request.Parameters.Clear();
                response = client.Execute(request);
            }

            return response;
        }
        public static string GetCompanyName(string agreementNumber)
        {
            iDB2Connection objConn;
            iDB2Command objCmd;
            try
            {
                log.Info($"...getting company information fro AS400...");
                string ConnectionString = ConfigurationManager.AppSettings["AS400"];
                objConn = new iDB2Connection(ConnectionString);
                objConn.Open();
                objCmd = new iDB2Command
                {
                    CommandText = $"SELECT XCOMPANY FROM BWGDATA.SMXREFFP WHERE XSACONT = '{agreementNumber}'",
                    CommandType = CommandType.Text,
                    Connection = objConn
                };
                var reader = objCmd.ExecuteReader();
                if (!reader.Read())
                {
                    return null;
                }
                return reader.GetString(0);
            }
            catch (Exception ex)
            {
                return null;
            }
            //finally
            //{
            //    objCmd.Dispose();
            //    objConn.Dispose();
            //}
        }
        public static string ExecuteHttp(string url)
        {
            log.Info($"...recreating file using API call...");
            var client = new WebClient();
            var content = client.DownloadString(url);
            return content;
        }
        public class Reprint
        {
            public string ORDER { get; set; }
            public string CONTRACT { get; set; }
            public string STATE { get; set; }
        }
    }
}
